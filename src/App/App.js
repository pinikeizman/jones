import React, {Component} from "react";
import {FlexBox} from "../Styled/styled";
import Form from "../Form";

class App extends Component {
    render() {
        return (
            <FlexBox alignItems="center" justifyContent="center" flexDirection="column" className="App">
                <Form/>
            </FlexBox>
        );
    }
}

export default App;
