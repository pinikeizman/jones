export const sendMail = ({from, to, subject, content}) => {
    const apiUrl = "https://api.sendgrid.com/v3/mail/send";
    // in production this key should not be on the client side.
    const api_key = "Bearer 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"
    return fetch(apiUrl, {
        method: 'POST',
        body: JSON.stringify({
            "personalizations": [
                {
                    "to": [
                        {
                            "email": to
                        }
                    ],
                    "subject": subject
                }
            ],
            "from": {
                "email": from
            },
            "content": [
                {
                    "type": "text/plain",
                    "value": content
                }
            ]
        }),
        headers: ({
            'Content-Type': 'application/json',
            'Authorization': api_key
        })
    }).then(response => {
        if (response.status != 200 && response.status != 202) {
            throw new Error();
        }
        return;
    });
}
